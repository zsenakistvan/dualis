-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Nov 26. 04:58
-- Kiszolgáló verziója: 8.0.18
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `istvan`
--
CREATE DATABASE IF NOT EXISTS `istvan` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `istvan`;

DELIMITER $$
--
-- Eljárások
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_car` (IN `rendszam_in` VARCHAR(6) CHARSET utf8, IN `marka_in` VARCHAR(200) CHARSET utf8, IN `driver_id_in` INT(8))  NO SQL
INSERT INTO `car` (`car`.`rendszam`, `car`.`marka`, `car`.`driver_id`) VALUE (rendszam_in, marka_in, driver_id_in)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_last_car_id` (OUT `id_out` INT(8))  NO SQL
SELECT MAX(`car`.`id`) INTO id_out FROM `car`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `read_car` (IN `id_in` INT(8))  NO SQL
SELECT * FROM `car` WHERE `car`.`id` = id_in$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `car`
--

CREATE TABLE `car` (
  `id` int(8) NOT NULL,
  `rendszam` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `marka` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `car`
--

INSERT INTO `car` (`id`, `rendszam`, `marka`, `driver_id`) VALUES
(2, 'SDP945', 'Audi', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `driver`
--

CREATE TABLE `driver` (
  `id` int(8) NOT NULL,
  `nev` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jogsi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vezethet` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `driver`
--

INSERT INTO `driver` (`id`, `nev`, `jogsi`, `vezethet`) VALUES
(1, 'Isti', '734685', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rendszam` (`rendszam`),
  ADD KEY `fk_car_driver` (`driver_id`);

--
-- A tábla indexei `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jogsi` (`jogsi`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `car`
--
ALTER TABLE `car`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `car`
--
ALTER TABLE `car`
  ADD CONSTRAINT `fk_car_driver` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
